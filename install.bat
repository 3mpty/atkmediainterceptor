@setlocal enableextensions
@cd /d "%~dp0"

ECHO OFF
CLS
ECHO Copying DMedia.exe to ProgramFiles...

COPY ".\bin\Release\DMedia.exe" "%HOMEDRIVE%\Program Files (x86)\ASUS\ATK Package\ATK Media\"

ECHO Done... Please restart your computer or run bin\Release\DMedia.exe manually
PAUSE
